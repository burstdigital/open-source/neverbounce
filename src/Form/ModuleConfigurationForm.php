<?php

namespace Drupal\neverbounce\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class ModuleConfigurationForm.
 *
 * @package Drupal\neverbounce\Form
 */
class ModuleConfigurationForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'neverbounce.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'neverbounce_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('neverbounce.settings');
    $form['api_username'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Username'),
      '#description' => $this->t('The API Username provided by NeverBounce'),
      '#default_value' => $config->get('api_username'),
      '#required' => TRUE,
    ];
    $form['api_secret_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Secret Key'),
      '#description' => $this->t('The API Secret Key provided by NeverBounce'),
      '#default_value' => $config->get('api_secret_key'),
      '#required' => TRUE,
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('neverbounce.settings')
      ->set('api_username', $form_state->getValue('api_username'))
      ->set('api_secret_key', $form_state->getValue('api_secret_key'))
      ->save();
  }

}
